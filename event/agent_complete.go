// Package event for AMI
package event

// AgentConnect triggered when an agent connects.
type AgentComplete struct {
	Privilege  []string
	HoldTime   string `AMI:"Holdtime"`
	TalkTime   string `AMI:"TalkTime"`
	MemberName string `AMI:"Membername"`
	Queue      string `AMI:"Queue"`
	UniqueID   string `AMI:"Uniqueid"`
	Channel    string `AMI:"Channel"`
	Reason     string `AMI:"Reason"`
	Interface  string `AMI:"Interface"`
}

func init() {
	eventTrap["AgentComplete"] = AgentComplete{}
}
