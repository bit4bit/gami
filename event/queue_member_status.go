// Package event for AMI
package event

//Raised when a Queue member's status has changed.
type QueueMemberStatus struct {
	Privilege      []string
	Queue          string `AMI:"Queue"`
	MemberName     string `AMI:"Membername"`
	InCall         int64  `AMI:"Incall"`
	Status         int64  `AMI:"Status"`
	Paused         int64  `AMI:"Paused"`
	PausedReason   string `AMI:"Pausedreason"`
	Ringinuse      int64  `AMI:"Ringinuse"`
	StateInterface string `AMI:"Stateinterface"`
}

func init() {
	eventTrap["QueueMemberStatus"] = QueueMemberStatus{}
}
