// Package event for AMI
package event

type QueueMember struct {
	Privilege      []string
	Name           string `AMI:"Name"`
	Queue          string `AMI:"Queue"`
	InCall         int64  `AMI:"Incall"`
	Status         int64  `AMI:"Status"`
	Paused         int64  `AMI:"Paused"`
	PausedReason   string `AMI:"Pausedreason"`
	StateInterface string `AMI:"Stateinterface"`
}

func init() {
	eventTrap["QueueMember"] = QueueMember{}
}
