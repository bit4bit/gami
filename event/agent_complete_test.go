package event

import (
	"testing"

	"gitlab.com/bit4bit/gami"
)

func TestAgentCompleteCasting(t *testing.T) {
	fixture := map[string]string{
		"Membername": "MemberName",
		"Queue":      "Queue",
		"Uniqueid":   "UniqueID",
		"Channel":    "Channel",
	}

	ev := gami.AMIEvent{
		ID:        "AgentComplete",
		Privilege: []string{"all"},
		Params:    fixture,
	}

	evtype := New(&ev)
	if _, ok := evtype.(AgentComplete); ok {
		t.Fatal("AgentConnect type assertion")
	}

	testEvent(t, fixture, evtype)
}
