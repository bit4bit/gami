// Package event for AMI
package event

type DBGetResponse struct {
	Privilege []string

	Family string `AMI:"Family"`
	Key    string `AMI:"Key"`
	Val    string `AMI:"Val"`
}

func init() {
	eventTrap["DBGetResponse"] = DBGetResponse{}
}
