// Package event for AMI
package event

type FullyBooted struct {
	Privilege []string

	LastReload int64  `AMI:"Lastreload"`
	Status     string `AMI:"Status"`
}

func init() {
	eventTrap["FullyBooted"] = FullyBooted{}
}
